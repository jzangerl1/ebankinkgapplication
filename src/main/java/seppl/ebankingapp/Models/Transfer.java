package seppl.ebankingapp.Models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Data
@NoArgsConstructor
public class Transfer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Nullable
    //@Size(min = 20, max = 20)
    private String senderIBAN;

    @Nullable
    private String senderBIC;

    //Bei Abhebung wird Minus-Betrag "empfangen"
    @NotNull
    //@Size(min = 20, max = 20)
    private String receiverIBAN;

    @NotNull
    private String receiverBIC;

    @NotNull
    private String payReference;

    @Nullable
    private String purpose;

    @NotNull
    private double amount;

    @Nullable
    //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-mm-dd")
    private LocalDate date;

    @Nullable
    private LocalTime time;

    @ManyToOne
    @Nullable
    @JsonIgnore
    private Account sender;

    @ManyToOne
    @Nullable
    @JsonIgnore
    private Account receiver;

}
