package seppl.ebankingapp.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.UniqueElements;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private Double balance;

    //@Size(min = 20, max = 20)
    @Column(unique = true)
    private String iban;

    @NotNull
    private String bic;

    @OneToMany(mappedBy = "sender", cascade = CascadeType.ALL)
    @NotNull
    private List<Transfer> sent;

    @OneToMany(mappedBy = "receiver", cascade = CascadeType.ALL)
    @NotNull
    private List<Transfer> received;

    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL)
    private User user;
}
