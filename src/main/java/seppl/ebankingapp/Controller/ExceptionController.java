package seppl.ebankingapp.Controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import seppl.ebankingapp.Exceptions.*;

@ControllerAdvice
@CrossOrigin(origins = "http://localhost:4200")
public class ExceptionController {
    @ExceptionHandler(UserNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ExceptionResponseEntity> handleUserNotFound (UserNotFoundException ex) {
        ExceptionResponseEntity exre = new ExceptionResponseEntity("1000",ex.getMessage());
        return new ResponseEntity<>(exre, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(DuplicateUserNameException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseEntity<ExceptionResponseEntity> handleDuplicateUsername (DuplicateUserNameException ex) {
        ExceptionResponseEntity exre = new ExceptionResponseEntity("1001",ex.getMessage());
        return new ResponseEntity<>(exre, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(TransferNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ExceptionResponseEntity> handleTransferNotFound (TransferNotFoundException ex) {
        ExceptionResponseEntity exre = new ExceptionResponseEntity("1001",ex.getMessage());
        return new ResponseEntity<>(exre, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(AmountNotValidException.class)
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    public ResponseEntity<ExceptionResponseEntity> handleAmountNotValid (AmountNotValidException ex) {
        ExceptionResponseEntity exre = new ExceptionResponseEntity("1001",ex.getMessage());
        return new ResponseEntity<>(exre, HttpStatus.NOT_ACCEPTABLE);
    }
}