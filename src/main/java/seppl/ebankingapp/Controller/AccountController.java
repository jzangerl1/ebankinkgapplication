package seppl.ebankingapp.Controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import seppl.ebankingapp.Exceptions.AccountNotFoundException;
import seppl.ebankingapp.Exceptions.TransferNotFoundException;
import seppl.ebankingapp.Models.Account;
import seppl.ebankingapp.Models.Transfer;
import seppl.ebankingapp.Services.AccountService;

import java.time.LocalDate;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class AccountController {

    AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/account")
    public ResponseEntity<List<Account>> getAllAccount() {
        return ResponseEntity.ok(accountService.getAllAccount());
    }

    @GetMapping("/account/{id}")
    public ResponseEntity<Account> getAccountById(@PathVariable Long id) throws AccountNotFoundException {
        return ResponseEntity.ok(accountService.getAccountById(id));
    }

    @PatchMapping("/account/{id}")
    public ResponseEntity<Account> updateAccount(@PathVariable Long id, @RequestBody Account account) throws AccountNotFoundException {
        return ResponseEntity.ok(accountService.updateAccount(id, account));
    }

    @DeleteMapping("/account/{id}")
    public void deleteAccount(@PathVariable Long id) throws AccountNotFoundException {
        accountService.deleteAccount(id);
    }

    @PostMapping("/account/{id}/date")
    public ResponseEntity<List<Transfer>> searchByDate(@PathVariable Long id, @RequestParam(name = "date") String date, @RequestParam(name = "date2") String date2) throws TransferNotFoundException, AccountNotFoundException {
        LocalDate localDate = LocalDate.parse(date);
        LocalDate localDate2 = LocalDate.parse(date2);
        return ResponseEntity.ok(accountService.searchByDate(id, localDate, localDate2));
    }

    @PostMapping("/account/{id}/amount")
    public ResponseEntity<List<Transfer>> searchByAmount(@PathVariable Long id, @RequestParam(name="amount") Double amount, @RequestParam(name="amount2") Double amount2) throws TransferNotFoundException, AccountNotFoundException {
        return ResponseEntity.ok(accountService.searchByAmount(id, amount, amount2));
    }

    @PostMapping("/account/{id}/text")
    public ResponseEntity<List<Transfer>> searchByText(@PathVariable Long id, @RequestParam(name="text") String text) throws TransferNotFoundException, AccountNotFoundException {
        return ResponseEntity.ok(accountService.searchByText(id, text));
    }
}
