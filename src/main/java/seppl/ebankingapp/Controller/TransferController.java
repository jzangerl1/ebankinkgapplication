package seppl.ebankingapp.Controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import seppl.ebankingapp.Exceptions.AccountNotFoundException;
import seppl.ebankingapp.Exceptions.AmountNotValidException;
import seppl.ebankingapp.Exceptions.TransferNotFoundException;
import seppl.ebankingapp.Models.Transfer;
import seppl.ebankingapp.Services.TransferService;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class TransferController {

    TransferService transferService;

    public TransferController (TransferService transferService){this.transferService = transferService;}

    @GetMapping("/transfer/{id}")
    public ResponseEntity<Transfer> getTransferById(@PathVariable Long id) throws TransferNotFoundException {
        return ResponseEntity.ok(transferService.getTransferById(id));
    }

    @GetMapping("/account/{id}/transfers")
    public ResponseEntity<List<Transfer>> getTransfersByAccount(@PathVariable Long id) throws AccountNotFoundException, TransferNotFoundException {
        return ResponseEntity.ok(transferService.getTransfersByAccount(id));
    }

    @PostMapping("/transfer")
    public ResponseEntity<Transfer> addTransfer(@RequestBody Transfer transfer) throws  AccountNotFoundException, AmountNotValidException {
        return ResponseEntity.ok(transferService.addTransfer(transfer));
    }
}
