package seppl.ebankingapp.Services;

import net.bytebuddy.utility.RandomString;
import org.springframework.stereotype.Service;
import seppl.ebankingapp.Exceptions.AccountNotFoundException;
import seppl.ebankingapp.Exceptions.DuplicateIbanException;
import seppl.ebankingapp.Exceptions.TransferNotFoundException;
import seppl.ebankingapp.Models.Account;
import seppl.ebankingapp.Models.Transfer;
import seppl.ebankingapp.Models.User;
import seppl.ebankingapp.Repositories.AccountRepository;
import seppl.ebankingapp.Repositories.TransferRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AccountService {
    AccountRepository accountRepository;
    TransferRepository transferRepository;

    public AccountService(AccountRepository accountRepository, TransferRepository transferRepository) {
        this.accountRepository = accountRepository;
        this.transferRepository = transferRepository;
    }

    /**
     * Gibt eine Liste mit allen Accounts zurück
     * @return Liste mit allen vorhandenen Accounts
     */
    public List<Account> getAllAccount() {
        return accountRepository.findAll();
    }

    /**
     * Fügt einen neuen Account zu einem User dazu
     * @param user Der user, der einen Account bekommt
     * @return Der Account, der erstellt wurde
     */
    public Account addAccount(User user) {
        Account newAccount = new Account();
        //newAccount.setIban("AT"+RandomString.make(18));
        newAccount.setIban("AT" + user.getUsername());
        Optional<Account> opaccount = accountRepository.findByIban(newAccount.getIban());
        if (!opaccount.isPresent()) {
            newAccount.setUser(user);
            newAccount.setBalance(0.0);
            newAccount.setBic("FAJAAT2BXXX");
            newAccount.setReceived(new ArrayList<>());
            newAccount.setSent(new ArrayList<>());
            return accountRepository.save(newAccount);
        } else {
            return addAccount(user);
        }
    }


    /**
     * Holt einen bestimmten Account nach id.
     * @param id id des Accounts, der gefunden werden sollte
     * @return Der Account der gefunden wurde
     * @throws AccountNotFoundException
     */
    public Account getAccountById(Long id) throws AccountNotFoundException {
        Optional<Account> optaccount = accountRepository.findById(id);
        if (optaccount.isPresent()) {
            return optaccount.get();
        } else {
            throw new AccountNotFoundException();
        }
    }

    /**
     * Updated einen Account
     * @param id Die id des zu ändernden Accounts
     * @param account Der Account mit neuen Daten
     * @return Der geupdatete Account
     * @throws AccountNotFoundException
     */
    public Account updateAccount(Long id, Account account) throws AccountNotFoundException {
        Optional<Account> optaccount = accountRepository.findById(id);
        if (optaccount.isPresent()) {
            Account updatedAccount = optaccount.get();
            updatedAccount.setId(account.getId());
            updatedAccount.setUser(account.getUser());
            updatedAccount.setIban(account.getIban());
            updatedAccount.setBic(account.getBic());
            updatedAccount.setBalance(account.getBalance());
            updatedAccount.setSent(account.getSent());
            updatedAccount.setReceived(account.getReceived());
            return accountRepository.save(updatedAccount);
        } else {
            throw new AccountNotFoundException();
        }
    }

    /**
     * Löscht einen Account nach id
     * @param id Die id des Accounts, der gelöscht werden soll
     * @throws AccountNotFoundException
     */
    public void deleteAccount(Long id) throws AccountNotFoundException {
        Optional<Account> optaccount = accountRepository.findById(id);
        if (optaccount.isPresent()) {
            accountRepository.delete(optaccount.get());
        } else {
            throw new AccountNotFoundException();
        }
    }

    /**
     * Sucht nach Transfers in einem Account, zwischen bestimmten Daten
     * @param id die id des Accounts, der durchsucht werden soll
     * @param localDate Das Anfangsdatum des zu durchsuchenden Zeitzuams
     * @param localDate2 Das Enddatum des zu durchsuchenden Zeitraums
     * @return Die Liste aller Transfers des Accounts in dem mitgegebenen Zeitraum
     * @throws TransferNotFoundException
     * @throws AccountNotFoundException
     */
    public List<Transfer> searchByDate(Long id, LocalDate localDate, LocalDate localDate2) throws TransferNotFoundException, AccountNotFoundException {
        Optional<Account> optaccount = accountRepository.findById(id);
        if (optaccount.isPresent()) {
            Account account = optaccount.get();
            Optional<List<Transfer>> opListTransfer = transferRepository.findAllBySenderIBANOrReceiverIBANAndDateBetween(account.getIban(), account.getIban(), localDate, localDate2);
            if (opListTransfer.isPresent()) {
                return opListTransfer.get();
            } else {
                throw new TransferNotFoundException();
            }
        } else {
            throw new AccountNotFoundException();
        }
    }


    /**
     * Sucht Transfers in einem Account, in denen die Beträge zwischen bestimmten Beträgen liegen
     * @param id Die id des Accounts, der durchsucht werden soll
     * @param amount Die untere Grenze des zu durchsuchenden Rahmens
     * @param amount2 Die obere Grenze des zu durchsuchenden Rahmens
     * @return Die Liste aller Transfers des Accounts in dem mitgegebenen Rahmen
     * @throws TransferNotFoundException
     * @throws AccountNotFoundException
     */
    public List<Transfer> searchByAmount(Long id, Double amount, Double amount2) throws TransferNotFoundException, AccountNotFoundException {
        Optional<Account> optAccount = accountRepository.findById(id);
        if (optAccount.isPresent()) {
            Optional<List<Transfer>> optListTransfer = transferRepository.findAllBySenderIBANOrReceiverIBANAndAmountBetween(optAccount.get().getIban(), optAccount.get().getIban(), amount, amount2);
            if (optListTransfer.isPresent()) {
                return optListTransfer.get();
            } else {
                throw new TransferNotFoundException();
            }
        } else {
            throw new AccountNotFoundException();
        }
    }

    /**
     * Durchsucht einen bestimmten Account nach Textinformationen
     * @param id Die id des Accounts, der durchsucht werden soll
     * @param text Der Text, nach dem gesucht werden soll
     * @return Alle Transfers, in denen der gesuchte Text vorkommt
     * @throws TransferNotFoundException
     * @throws AccountNotFoundException
     */
    public List<Transfer> searchByText(Long id, String text) throws TransferNotFoundException, AccountNotFoundException{
        Optional<Account> optAccount = accountRepository.findById(id);
        if(optAccount.isPresent()){
            Optional<List<Transfer>> optListTransfer = transferRepository.findAllBySenderIBANOrReceiverIBANAndPayReferenceOrPurpose(optAccount.get().getIban(), optAccount.get().getIban(), text, text);
            if(optListTransfer.isPresent()){
                return optListTransfer.get();
            } else {
                throw new TransferNotFoundException();
            }
        } else {
            throw new AccountNotFoundException();
        }
    }
}
