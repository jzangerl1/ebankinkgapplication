package seppl.ebankingapp.Services;

import org.springframework.stereotype.Service;
import seppl.ebankingapp.Exceptions.AccountNotFoundException;
import seppl.ebankingapp.Exceptions.AmountNotValidException;
import seppl.ebankingapp.Exceptions.TransferNotFoundException;
import seppl.ebankingapp.Models.Account;
import seppl.ebankingapp.Models.Transfer;
import seppl.ebankingapp.Repositories.AccountRepository;
import seppl.ebankingapp.Repositories.TransferRepository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@Service
public class TransferService {
    TransferRepository transferRepository;
    AccountRepository accountRepository;

    public TransferService(TransferRepository transferRepository, AccountRepository accountRepository) {
        this.transferRepository = transferRepository;
        this.accountRepository = accountRepository;
    }

    /**
     * Holt einen Transfer nach id
     * @param id Die id des Transfers, der gesucht wird
     * @return Der Transfer, der mit der id gefunden wurde
     * @throws TransferNotFoundException
     */
    public Transfer getTransferById(Long id) throws TransferNotFoundException {
        Optional<Transfer> opTransfer = transferRepository.findById(id);
        if (opTransfer.isPresent()) {
            return opTransfer.get();
        } else {
            throw new TransferNotFoundException();
        }
    }

    /**
     * Holt alle Transfers für einen bestimmten Account
     * @param id Die id des Accounts, für den die Transfers gesucht werden sollen
     * @return Die Transfers, die für den Account gefunden wurden.
     * @throws AccountNotFoundException
     * @throws TransferNotFoundException
     */
    public List<Transfer> getTransfersByAccount(Long id) throws AccountNotFoundException, TransferNotFoundException {
        Optional<Account> opAccount = accountRepository.findById(id);
        Optional<List<Transfer>> opTransfersReceiver;
        Optional<List<Transfer>> opTransfersSender;
        if (opAccount.isPresent()) {
            opTransfersReceiver = transferRepository.findByReceiver(opAccount.get());
            opTransfersSender = transferRepository.findBySender(opAccount.get());
            if (opTransfersReceiver.isPresent() || opTransfersSender.isPresent()) {
                List<Transfer> transfers = opTransfersReceiver.get();
                transfers.addAll(opTransfersSender.get());
                return transfers;
            } else {
                throw new TransferNotFoundException();
            }

        } else {
            throw new AccountNotFoundException();
        }
    }

    /**
     * Fügt einen neuen Transfer hinzu
     * @param transfer Der Transfer, der hinzugefügt werden soll
     * @return Der Transfer, der hinzugefügt wurde
     * @throws AccountNotFoundException
     * @throws AmountNotValidException
     */
    public Transfer addTransfer(Transfer transfer) throws AccountNotFoundException, AmountNotValidException {
        transfer.setDate(LocalDate.now());
        transfer.setTime(LocalTime.now());
        Optional<Account> opAccountReceiver = accountRepository.findByIban(transfer.getReceiverIBAN());
        Optional<Account> opAccountSender = accountRepository.findByIban(transfer.getSenderIBAN());
        if (opAccountReceiver.isPresent() && opAccountSender.isPresent()) {
            Account receiver = opAccountReceiver.get();
            Account sender = opAccountSender.get();
            sender.setBalance(sender.getBalance() - transfer.getAmount());
            receiver.setBalance(receiver.getBalance() + transfer.getAmount());
            sender.getSent().add(transfer);
            receiver.getReceived().add(transfer);
            transfer.setReceiver(receiver);
            transfer.setSender(sender);
        } else if (opAccountReceiver.isPresent()) {
            Account receiver = opAccountReceiver.get();
            if (transfer.getAmount() > 0) {
                receiver.setBalance(receiver.getBalance() + transfer.getAmount());
                receiver.getReceived().add(transfer);
                transfer.setPayReference("Einzahlung");
                transfer.setReceiver(receiver);
            } else if (transfer.getAmount() < 0) {
                receiver.setBalance(receiver.getBalance() + transfer.getAmount());
                receiver.getSent().add(transfer);
                transfer.setPayReference("Behebung");
                transfer.setSender(receiver);
            } else {
                throw new AmountNotValidException();
            }


        } else {
            throw new AccountNotFoundException();
        }


        return transferRepository.save(transfer);
    }
}
