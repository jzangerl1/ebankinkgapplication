package seppl.ebankingapp.Services;

import org.springframework.stereotype.Service;
import seppl.ebankingapp.Exceptions.DuplicateUserNameException;
import seppl.ebankingapp.Exceptions.UserNotFoundException;
import seppl.ebankingapp.Models.Account;
import seppl.ebankingapp.Models.User;
import seppl.ebankingapp.Repositories.UserRepository;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    UserRepository userRepository;
    AccountService accountService;

    public UserService(UserRepository userRepository, AccountService accountService){
        this.userRepository = userRepository;
        this.accountService = accountService;
    }

    /**
     * Fügt einen neuen User hinzu und gibt ihm die Rolle User
     * @param user Der User, der hinzugefügt werden soll
     * @return Der User der hinzugefügt wurde
     * @throws DuplicateUserNameException
     */
    public User addUser(User user) throws DuplicateUserNameException{
        if(!userRepository.findByUsername(user.getUsername()).isPresent()) {
            user.setAccount(accountService.addAccount(user));
            user.getRoles().add("ROLE_USER");
            return userRepository.save(user);
        }
        else{
            throw new DuplicateUserNameException();
        }
    }

    /**
     * Gibt alle User zurück, die sich im System befinden
     * @return Die Liste aller User
     */
    public List<User> getAllUser() {
        return userRepository.findAll();
    }

    /**
     * Gibt einen User nach id zurück
     * @param id die id des Users, der gesucht wird
     * @return Der gefundene User
     * @throws UserNotFoundException
     */
    public User getUserById(Long id) throws UserNotFoundException {
        Optional<User> opuser = userRepository.findById(id);
        if(opuser.isPresent()){
            return opuser.get();
        }
        else{
            throw new UserNotFoundException();
        }
    }

    /**
     * Updated den User
     * @param id Die id des Users, der geupdated werden soll
     * @param user Der User mit den neuen Daten
     * @return Der geupdatete User
     * @throws UserNotFoundException
     */
    public User updateUser(Long id, User user) throws UserNotFoundException{
        Optional<User> opuser = userRepository.findById(id);
        if(opuser.isPresent()){
            User updatedUser = opuser.get();
            updatedUser.setUsername(user.getUsername());
            updatedUser.setFirstname(user.getFirstname());
            updatedUser.setLastname(user.getLastname());
            updatedUser.setPassword(user.getPassword());
            return userRepository.save(updatedUser);
        }
        else{
            throw new UserNotFoundException();
        }
    }

    /**
     * Löscht einen User
     * @param id Die id des Users, der gelöscht werden soll
     * @throws UserNotFoundException
     */
    public void deleteUser(Long id) throws UserNotFoundException {
        Optional<User> opuser = userRepository.findById(id);
        if(opuser.isPresent()){
            userRepository.delete(opuser.get());
        }
        else{
            throw new UserNotFoundException();
        }
    }
}
