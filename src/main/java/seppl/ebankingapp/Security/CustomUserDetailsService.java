package seppl.ebankingapp.Security;

import org.springframework.security.core.userdetails.*;
import org.springframework.stereotype.Component;
import seppl.ebankingapp.Repositories.UserRepository;

@Component
public class CustomUserDetailsService implements UserDetailsService {

    private UserRepository users;

    public CustomUserDetailsService(UserRepository users) {
        this.users = users;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return this.users.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Username: " + username + " not found"));
    }
}