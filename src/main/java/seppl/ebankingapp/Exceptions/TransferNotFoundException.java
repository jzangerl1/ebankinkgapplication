package seppl.ebankingapp.Exceptions;

public class TransferNotFoundException extends Exception {
    public TransferNotFoundException() {
        super("Angefragter Transfer nicht vorhanden");
    }
}
