package seppl.ebankingapp.Exceptions;

public class AmountNotValidException extends Exception {
    public AmountNotValidException (){super("Betrag ist nicht gültig!");}
}
