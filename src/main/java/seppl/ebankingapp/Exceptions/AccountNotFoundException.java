package seppl.ebankingapp.Exceptions;

public class AccountNotFoundException extends Exception{

    public AccountNotFoundException() {
        super("Angefragter Account nicht vorhanden");
    }
}
