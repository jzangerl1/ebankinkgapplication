package seppl.ebankingapp.Exceptions;

public class DuplicateIbanException extends Throwable {

    public DuplicateIbanException()
    {
        super("IBAN bereits in DB vorhanden, generiere neue!");
    }
}
