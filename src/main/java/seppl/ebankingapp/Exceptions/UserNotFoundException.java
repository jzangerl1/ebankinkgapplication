package seppl.ebankingapp.Exceptions;

public class UserNotFoundException extends Exception {

    public UserNotFoundException() {
        super("Angefragter Benutzer nicht vorhanden");
    }
}
