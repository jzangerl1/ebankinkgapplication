package seppl.ebankingapp.Repositories;

import org.apache.tomcat.jni.Local;
import org.springframework.data.jpa.repository.JpaRepository;
import seppl.ebankingapp.Models.Account;
import seppl.ebankingapp.Models.Transfer;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;


public interface TransferRepository extends JpaRepository<Transfer, Long> {
    Optional<List<Transfer>> findBySender(Account account);
    Optional<List<Transfer>> findByReceiver(Account account);

    Optional<List<Transfer>> findAllBySenderIBANOrReceiverIBANAndDateBetween(String sender, String receiver, LocalDate localDate, LocalDate localDate2);
    Optional<List<Transfer>> findAllBySenderIBANOrReceiverIBANAndAmountBetween (String sender, String receiver, Double amount, Double amount2);
    Optional<List<Transfer>> findAllBySenderIBANOrReceiverIBANAndPayReferenceOrPurpose (String sender, String receiver, String payReference, String purpose);
    //Optional<List<Transfer>> findAllByBySenderIBANOrReceiverIBANAndPayReferenceContainsOrPurposeContains (String sender, String receiver, String payReference, String purpose);
}
