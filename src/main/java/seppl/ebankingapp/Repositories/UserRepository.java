package seppl.ebankingapp.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import seppl.ebankingapp.Models.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
}