package seppl.ebankingapp.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import seppl.ebankingapp.Models.Account;
import seppl.ebankingapp.Models.Transfer;
import seppl.ebankingapp.Models.User;

import java.util.List;
import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {
    Optional<Account> findByIban(String iban);
    Optional<Account> findByuser(User user);
}
